import React, { Component } from 'react'
import { Button,
         Input,
         Header,
         Icon,
         Card,
         Image,
         Reveal,
         Dropdown,
         Menu,
         Pagination
       } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import copy from 'copy-to-clipboard'
import axios from 'axios'
import { Modal, Carousel } from 'react-bootstrap'

import styles from './Search.scss'

var productInfo = require('../../assets/product_info.json');
var productInfo = [];

import DropdownGroup from './DropdownGroup.jsx';

/**************************************Render list*********************************/
/**********************************************************************************/
class ProductLst extends Component {

  constructor(props){
    super(props);
    this.state={
      activePage:1,
      show: false,
    }
    this.copyLink = this.copyLink.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
    this.handleHide = this.handleHide.bind(this);
    this.showMatchingOptions = this.showMatchingOptions.bind(this);
  }

  copyLink(event, data){
    var copy_txt = data.id;
    copy(copy_txt);
    alert('Copied!');
  }

  onPageChange(e, {activePage}){
    this.setState({
      activePage: activePage
    });
  }

  handleHide() {
    this.setState({
      show: false
    });
  }

  showMatchingOptions(){
    this.setState({
      show: true
    });
  }

  render() {
    if (this.props.productLst.length != 0){
      console.log('We can render products now!');
      var renderPdtLst = this.props.productLst[this.state.activePage-1];
      console.log('Render list is:');
      console.log(renderPdtLst);
      return(
        <div>
          <Card.Group itemsPerRow={4}>
            {renderPdtLst.map((pdt,idx)=>
              <Card key={idx} centered >

              <Carousel slide={false}>
                <Carousel.Item>
                  <Image src={pdt.image_urls[0]}/>
                </Carousel.Item>
                <Carousel.Item>
                  <Image src={pdt.image_urls[1]}/>
                </Carousel.Item>
                <Carousel.Item>
                  <Image src={pdt.image_urls[2]}/>
                </Carousel.Item>
                <Carousel.Item>
                  <Image src={pdt.image_urls[3]}/>
                </Carousel.Item>
              </Carousel>

              <Modal
                show={this.state.show}
                onHide={this.handleHide}
                container={this}
                aria-labelledby="contained-modal-title"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="contained-modal-title">
                    Matching Suggestions
                  </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <Image src={"../../assets/placeholder-image.png"}/>
                  <p>This is a placeholder for a lists of images.</p>
                </Modal.Body>
                <Modal.Footer>
                  <Button onClick={this.handleHide}>Close</Button>
                </Modal.Footer>
              </Modal>

              <Card.Content>
                <Card.Header>{pdt.name}</Card.Header>
                <Card.Meta>{pdt.website}</Card.Meta>
                <Card.Description>{pdt.description}</Card.Description>
              </Card.Content>
              <Card.Content extra>
                <Button basic id={pdt.link} onClick={this.copyLink}>Copy Link</Button>
                <Button basic id={pdt.link} onClick={this.showMatchingOptions}>Matching</Button>
              </Card.Content>
            </Card>
          )}</Card.Group>
        </div>
      )
    } else {
      return null;
    }
  }
}

/******************************* Search *********************************************/
/************************************************************************************/

class Search extends Component {

    constructor(props){
      super(props);
      this.state={
        activeItem: 'Search',
        productLst: [[]], //a list of lists, default is the first list
        input_productLst: [], //a list of products only filtered by input_str
        page_number_upperbound: 1,
      }
      this.inputChangeHandler = this.inputChangeHandler.bind(this);
      this.changeGenderHandler = this.changeGenderHandler.bind(this);
      //this.changePriceHandler = this.changePriceHandler.bind(this);

      this.navBarHandler = this.navBarHandler.bind(this);
      this.logoutHandler = this.logoutHandler.bind(this);
    }

/*************************************Clean LocalStorage*****************************/
    componentDidMount(){
      localStorage.clear();
    }

/*************************************Input Search***********************************/
    inputChangeHandler(event){
      var filtered_data = []; //temp array to store filtered results
      var input_str = event.target.value; //input string
      var raw_database = localStorage.getItem('data'); //get stored data, json string
      var page_number_upperbound = 0;

      if (input_str == ''){
        //do nothing on renderPdtLst
        //reset all parameters
        this.setState({
          productLst: []
        });
      } else {
        var input_words = input_str.split(' '); //List containing all input words

        if (!raw_database){
          //Local database is empty, call api and store response.data locally
          //* We want local data to be a product list, this is slightly different from backend data
          axios.get('https://us-central1-psbot-503dd.cloudfunctions.net/queryProducts?limit=2000')
            .then((response) => {
              console.log('Data from axios call: ');
              console.log(response.data);

              //The product from backend has a 'value' key, get rid of it
              var database = [];
              for (var i=0; i<response.data.length; i++){
                database.push(response.data[i].value);
              }

              console.log('Database now should not be empty');
              console.log(database);

              //Set local database
              localStorage.setItem('data', JSON.stringify(database));

              console.log('If anything goes wrong here:');
              console.log('1.Some product"s desciption is "[]", which should be an empty string');
              console.log('2.Some product attribute is undefined');
              //filt through database
              for (var i=0; i<database.length; i++){
                var product = database[i];
                var positive_sum = 0;
                try {
                  for (var j=0; j<input_words.length; j++){
                    var input_word = input_words[j];
                    if (
                        product.designer.toLowerCase().includes(input_word.toLowerCase()) ||
                        product.name.toLowerCase().includes(input_word.toLowerCase()) ||
                        product.product_type.toLowerCase().includes(input_word.toLowerCase())
                      ){
                        positive_sum ++;
                      }
                  }
                  for (var k=0; k<product.colors.length; k++){
                    if (product.colors[k].toLowerCase() == input_word){
                      positive_sum ++;
                    }
                  }
                }
                catch (error){
                  console.log(error.message);
                }
                if (positive_sum >= input_words.length){
                  filtered_data.push(product);
                }
              }
              //set a total page number
              page_number_upperbound = filtered_data.length/40; //40 is the default display number per page
              page_number_upperbound = Math.ceil(page_number_upperbound);

              //put data into a list of lists for all pages: each page get one corresponding list
              var filtered_lists = [];
              var list = [];
              var idx = 0;
              for (var h=0; h<filtered_data.length; h++){
                if (idx == 40){
                  filtered_lists.push(list)
                  idx = 0;
                  list = [];
                }
                list.push(filtered_data[h]);
                idx ++;
              }
              if (idx > 0){ //edge case when there are some data (<40) left in the end
                filtered_lists.push(list);
              }
              if (filtered_data.length < 40){ //edge case when all data is less than 40
                filtered_lists.push(list);
              }

              this.setState({
                page_number_upperbound: page_number_upperbound,
                productLst: filtered_lists,
                input_productLst: filtered_data
              }, function(){
                console.log('Input change data: ');
                console.log(this.state.productLst);
              });
            });
        } else {
          //Parse json value
          var database = JSON.parse(raw_database);
          console.log('Database now is: ');
          console.log(database);
          console.log('If anything goes wrong here:');
          console.log('1.Some product"s desciption is "[]", which should be an empty string');
          console.log('2.Nordstrom data are DUMB! They are all undefined!!!');
          //We have local database, filt throught it to get output
          for (var i=0; i<database.length; i++){
            var product = database[i];
            var positive_sum = 0;

            for (var j=0; j<input_words.length; j++){
              var input_word = input_words[j];
              try {
                if (
                    product.designer.toLowerCase().includes(input_word.toLowerCase()) ||
                    product.name.toLowerCase().includes(input_word.toLowerCase()) ||
                    product.product_type.toLowerCase().includes(input_word.toLowerCase())
                  ){
                    positive_sum ++;
                  }
                for (var k=0; k<product.colors.length; k++){
                  if (product.colors[k].toLowerCase() == input_word){
                    positive_sum ++;
                  }
                }
              }
              catch (error){
                console.log(error.message);
              }
            }
            if (positive_sum >= input_words.length){
              filtered_data.push(product);
            }
          }
          //set a total page number
          page_number_upperbound = filtered_data.length/40;
          page_number_upperbound = Math.ceil(page_number_upperbound);

          //put data into a list of lists for all pages: each page get one corresponding list
          var filtered_lists = [];
          var list = [];
          var idx = 0;
          for (var h=0; h<filtered_data.length; h++){
            if (idx == 40){
              filtered_lists.push(list)
              idx = 0;
              list = [];
            }
            list.push(filtered_data[h]);
            idx ++;
          }
          if (idx > 0){ //edge case when there are some data (<40) left in the end
            filtered_lists.push(list);
          }
          if (filtered_data.length < 40){ //edge case when all data is less than 40
            filtered_lists.push(list);
          }
          console.log('List of lists: ');
          console.log(filtered_lists);

          this.setState({
            page_number_upperbound: page_number_upperbound,
            productLst: filtered_lists,
            input_productLst: filtered_data
          }, function(){
            console.log('Input change data: ');
            console.log(this.state.productLst);
            console.log('Page number upper bound is: ');
            console.log(this.state.page_number_upperbound);
          });
        }
      }
    }

/****************************************Gender Filter************************************/
    changeGenderHandler(event, data){
      var gender = data.value;
      var filtered_data = []; //List of all filtered products from productLst
      var page_number_upperbound = 0;

      //check if input is empty (nothing in this.state.productLst)
      if (this.state.input_productLst.length == 0){  //call api w.r.t price
        axios.get('http://us-central1-psbot-503dd.cloudfunctions.net/queryProducts?gender='+gender)
          .then((response) => {
            console.log('Data from axios call: ');
            console.log(response.data);

            //The product from backend has a 'value' key, get rid of it
            var gender_database = [];
            for (var i=0; i<response.data.length; i++){
              gender_database.push(response.data[i].value);
            }

            console.log('Gender database now should not be empty');
            console.log(gender_database);

            //Set local database
            localStorage.setItem('gender_data', JSON.stringify(gender_database));

            //put all items from list of lists to a single last with price filtering
            filtered_data = gender_database;

            //transform list into a list of lists in order to set productLst
            page_number_upperbound = filtered_data.length/40;
            page_number_upperbound = Math.ceil(page_number_upperbound);
            //put data into a list of lists for all pages: each page get one corresponding list
            var filtered_lists = [];
            var list = [];
            var idx = 0;
            for (var h=0; h<filtered_data.length; h++){
              if (idx == 40){
                filtered_lists.push(list)
                idx = 0;
                list = [];
              }
              list.push(filtered_data[h]);
              idx ++;
            }
            if (idx > 0){ //edge case when there are some data (<40) left in the end
              filtered_lists.push(list);
            }
            if (filtered_data.length < 40){ //edge case when all data is less than 40
              filtered_lists.push(list);
            }
            console.log('(Gender) List of lists: ');
            console.log(filtered_lists);

            this.setState({
              page_number_upperbound: page_number_upperbound,
              productLst: filtered_lists
            }, function(){
              console.log('Gender change data: ');
              console.log(this.state.productLst);
              console.log('Page number upper bound is: ');
              console.log(this.state.page_number_upperbound);
            });

          });
      } else {  //filter through price in productLst
        //put all items from list of lists to a single last with price filtering
        try {
          for (var i=0; i<this.state.input_productLst.length; i++){
            var temp_product = this.state.input_productLst[i];
            if (temp_product.gender == gender){
              filtered_data.push(temp_product);
            }
          }
        }
        catch (error){
          console.log(error.message);
        }

        //transform list into a list of lists in order to set productLst
        page_number_upperbound = filtered_data.length/40;
        page_number_upperbound = Math.ceil(page_number_upperbound);
        //put data into a list of lists for all pages: each page get one corresponding list
        var filtered_lists = [];
        var list = [];
        var idx = 0;
        for (var h=0; h<filtered_data.length; h++){
          if (idx == 40){
            filtered_lists.push(list)
            idx = 0;
            list = [];
          }
          list.push(filtered_data[h]);
          idx ++;
        }
        if (idx > 0){ //edge case when there are some data (<40) left in the end
          filtered_lists.push(list);
        }
        if (filtered_data.length < 40){ //edge case when all data is less than 40
          filtered_lists.push(list);
        }
        console.log('(Gender) List of lists: ');
        console.log(filtered_lists);

        this.setState({
          page_number_upperbound: page_number_upperbound,
          productLst: filtered_lists
        }, function(){
          console.log('Gender change data: ');
          console.log(this.state.productLst);
          console.log('Page number upper bound is: ');
          console.log(this.state.page_number_upperbound);
        });
      }
    }

/*********************************Search, Tagging Switch*****************************/
    navBarHandler(e, {name}){
      window.location = '/#/' + name;
      this.setState({
        activeItem: name
      });
    }

/*********************************Logout**********************************************/
    logoutHandler(e, {name}){
      window.location = '/#/';
      this.setState({
        activeItem: name
      });
    }

/*********************************Main Render*****************************************/
    render() {
        return(
          <div className='main'>
            <div className='nav'>
              <Menu pointing secondary>
                <Menu.Item name='Search' active={this.state.activeItem === 'Search'} onClick={this.navBarHandler} />
                <Menu.Item name='Tagging' active={this.state.activeItem === 'Tagging'} onClick={this.navBarHandler} />
                <Menu.Menu position='right'>
                  <Menu.Item name='Logout' active={this.state.activeItem === 'Logout'} onClick={this.logoutHandler} />
                </Menu.Menu>
              </Menu>
            </div>
            <div className="Search">
              <Header as='h2' color='black'>Type your ideas...</Header>
              <div className="search-area">
                <div className="input">
                  <Input
                    onChange={this.inputChangeHandler} //inputHandler search pokeforms to match input
                    placeholder="Find a product ..."/>
                </div>
                <div className="dropdown-and-pagination">
                  <DropdownGroup products={this.state.productLst}/>
                  <div className='page-bar-container'>
                    <Pagination activePage={this.state.activePage} onPageChange={this.onPageChange} totalPages={this.props.page} />
                  </div>
                </div>
              </div>
            </div>
            <div className='result-container'>
              <ProductLst className='result-item' productLst={this.state.productLst} page={this.state.page_number_upperbound}/>
            </div>
          </div>
        )
    }
}

export default Search
